package com.sbradleyhoward.utils.coding.algorithmcodevault;

/**
 * 	Author: S Bradley Howard (friends call me Brad)
 * 	Website:	sbradleyhoward.com (Password: showme) 
 * 
 */



import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.TimeZone;
import java.util.concurrent.ConcurrentHashMap;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.text.DefaultEditorKit;

public class AlgorithmCodeVault extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static AlgorithmCodeVault frame;
	
	private JPanel contentPane;
	private JPanel panelsearch;
	private JPanel panelmain;
	private JPanel panelfooter;
	
	private JTextField txtsearch;
	
	private JButton btnNewSnippet;
	private JButton btnRemoveSnippet;
	
	private JScrollPane scrollPane;
	
	private static JTextArea textarea;
	
	private static JComboBox comboBox;
	
	private JMenuBar menuBar;
	
	private JMenu mnFile;
	private JMenuItem mnrebuildsearchlist;
	private JMenuItem mnHowManySnip;

	private static ArrayList<String> keywordlist;
	private static ArrayList<String> searchlist = null;
	private static ArrayList<Output2> openwindows = new ArrayList<Output2>();
	
	private static ConcurrentHashMap<String,Snippet> themap = null;
	
	private static String objectfilepath;
	
	private static Output2 output = null;
	
	private static volatile Snippet snippet = null;
	
	public static volatile boolean exiting = false;

	// keyword list for text coloring of Java code
	private static String[] javakeywords = new String[] {"abstract","assert","boolean","break","byte","case","catch","char","class","const",
													"continue","default","do","double","else","enum","extends","false","final","finally",
													"float","for","goto","if","implements","import","instanceof","int","interface","long",
													"native","new","package","private","protected","public","return","short","static",
													"strictfp","super","switch","synchronized","this","throw","throws","transient","true",
													"try","void","volatile","while"};
	
	// keyword list for text coloring of Java code
	private static String[] ckeywords = new String[] {"auto","break","case","char","const","continue","default","define","do","double","else",
													"endif","enum","external","float","for","goto","if","include","int","long","register","return","short",
													"signed","sizeof","static","struct","switch","typedef","union","unsigned","void",
													"volatile","while"};
	
	


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		//run program on EventDispatchThread
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {

					//Apple Specific stuff
					System.setProperty("com.apple.mrj.application.apple.menu.about.name", "FileIndexer");
					System.setProperty("apple.laf.useScreenMenuBar", "true");
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

					//create and display GUI
					frame = new AlgorithmCodeVault();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	//==============================================================================================================

	/**
	 * Create the frame.
	 */
	public AlgorithmCodeVault() {

		// look for the Vault path in the program's preference file
		getPrefs();


		//==============================================================================================================

		//gets array of snippet names for display/choosing and the map of the snippets
		new ACV(objectfilepath);

		//==============================================================================================================

		// this listener is used to do the file closing/saving when the user exits the program
		this.addWindowListener(new WindowListener()
		{
			@Override
			public void windowClosing(WindowEvent arg0) {
				exitProgram();
				return;
			}
			
			@Override
			public void windowClosed(WindowEvent arg0) {
				System.exit(0);
			}

			// unused overrides
			@Override
			public void windowActivated(WindowEvent arg0) {}
			@Override
			public void windowDeactivated(WindowEvent arg0) {}
			@Override
			public void windowDeiconified(WindowEvent arg0) {}
			@Override
			public void windowIconified(WindowEvent arg0) {}
			@Override
			public void windowOpened(WindowEvent arg0) {}

		});

		//==============================================================================================================

		//sets selection window in corner to allow room for the viewer window/windows to the right
		setBounds(50, 50, 360, 557);

		//==============================================================================================================

		//menu bar stuff
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		//+++++++++++++++

		JMenuItem location = new JMenuItem("New Vault Location");
		location.setToolTipText("Change where your vault is kept");
		location.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) {
				updatePrefs();				
			}
		});

		mnrebuildsearchlist = new JMenuItem("Rebuild Listing");
		mnrebuildsearchlist.setToolTipText("Rebuild this list from the Vault entries");
		mnrebuildsearchlist.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				rebuildSearchList();				
			}

		});
		
		//+++++++++++++++
		
		mnHowManySnip = new JMenuItem("How Many Snippets?");
		mnHowManySnip.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if( themap == null)
				{
					JOptionPane.showMessageDialog(null, "There are ZERO Snippets because\nthe Map is NULL");
				}
				else if( themap.size() == 1)
				{
					JOptionPane.showMessageDialog(null, "There is 1 Snippet");
				}
				else
				{
					JOptionPane.showMessageDialog(null, "There are "+themap.size()+ " Snippets");
				}
				
			}
			
		});
		
		//+++++++++++++++

		mnFile.add(location);
		mnFile.add(mnrebuildsearchlist);
		mnFile.add(mnHowManySnip);

		//==============================================================================================================

		// selection window stuff
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		panelsearch = new JPanel();
		panelsearch.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
		panelsearch.setLayout(new BorderLayout(0, 0));

		//==============================================================================================================

		// search box
		txtsearch = new JTextField();
		txtsearch.setText("Search for Snippet");
		txtsearch.setColumns(10);

		//if user hits Enter key in search box, do a search
		txtsearch.addKeyListener(new KeyListener()
		{
			@Override
			public void keyTyped(KeyEvent key) {
				if( key.getKeyChar() == '\n')
				{
					out(txtsearch.getText());
					startSearch(txtsearch.getText());
				}
			}

			// unused overrides
			@Override
			public void keyPressed(KeyEvent arg0) {}
			@Override
			public void keyReleased(KeyEvent arg0) {}
		});

		//+++++++++++++++

		txtsearch.addMouseListener(new MouseListener()
		{
			@Override
			public void mouseEntered(MouseEvent arg0) {
				txtsearch.setText("");				
			}
			@Override
			public void mouseClicked(MouseEvent arg0) {}
			@Override
			public void mouseExited(MouseEvent arg0) {}
			@Override
			public void mousePressed(MouseEvent arg0) {}
			@Override
			public void mouseReleased(MouseEvent arg0) {}

		});

		//==============================================================================================================

		// add snippet button
		btnNewSnippet = new JButton("New Snippet");

		//process click on button
		btnNewSnippet.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String s = "";
				while( s.equals("") == true)
				{
					s = JOptionPane.showInputDialog("Name your Snippet");
					//user hit cancel
					if( s == null)
						return;
				}
				//name seems okay. Duplicate name error is caught in makeSnippet()
				txtsearch.setText(s);
				makeSnippet();				
			}

		});

		//==============================================================================================================

		// delete snippet from map
		btnRemoveSnippet = new JButton("Remove Snippet");

		// process click
		btnRemoveSnippet.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e) {

				String selection = textarea.getSelectedText();
				if( selection == null || selection.equals(""))
				{
					if( themap.size() == 0)
					{
						JOptionPane.showMessageDialog(null, "The vault is empty Bozo\nso you have nothing to delete.");
						return;
					}
					selection = "";
					while( selection.equals(""))
					{
						selection = JOptionPane.showInputDialog("Which Snippet do you want to delete?");
						//user canceled delete
						if( selection == null)
							return;
					}
				}

				// try to find the snippet in map
				Snippet sn = themap.remove(selection);
				if( sn == null)
					JOptionPane.showMessageDialog(null, "Check spelling. That name not in map");
				// found a matching name
				else
				{
					// remove from list used to display to user in small window
					searchlist.remove(selection);
					textarea.setText("");

					// re-populate GUI with updated list
					populateList(0);

					// iterate through open snippet windows and close the deleted one
					int index = -1;
					for(Output2 t : openwindows)
					{
						index++;
						if( selection.equals(t.getName()) == true)
						{
							t.dispose();
							break;
						}
					}

					// remove deleted snippet/window from list
					if(index > -1)
						updateOpenWindowList(null,true,index);


					JOptionPane.showMessageDialog(null, selection+" has been deleted.");
				}
			}

		});

		//==============================================================================================================

		// GUI elements
		panelmain = new JPanel();
		panelmain.setLayout(new BorderLayout(0, 0));

		textarea = new JTextArea();
		textarea.setBackground(Color.LIGHT_GRAY);

		scrollPane = new JScrollPane();
		panelmain.add(scrollPane);
		scrollPane.setViewportView(textarea);

		//+++++++++++++++++

		//this is to "auto-select" whole line in Snippet list to ensure I'll get the whole line as input to the actionListener
		textarea.addMouseListener(new MouseListener()
		{
			//get default line selection for this OS
			Action selectline = getAction(DefaultEditorKit.selectLineAction);
			private Action getAction(String name)
			{
				Action action = null;
				Action[] actions = textarea.getActions();
				for( int a = 0; a < actions.length; a++)
				{
					// Action.NAME is the OS line selection method name. At least that is my understanding
					// finds the line selection name for this OS in the array of available actions
					if( name.equals( actions[a].getValue(Action.NAME).toString() ) )
					{
						action = actions[a];
						break;
					}
				}				
				return action;				
			}

			@Override
			public void mouseClicked(MouseEvent arg) {
				// user clicks on line to select a Snippet
				if( SwingUtilities.isLeftMouseButton(arg) && arg.getClickCount() == 1)
				{
					selectline.actionPerformed(null);

					// got snippet name selected, so find it
					startSearch( textarea.getSelectedText() );
				}


			}
			// unused overrides
			@Override
			public void mouseEntered(MouseEvent arg0) {}
			@Override
			public void mouseExited(MouseEvent arg0) {}
			@Override
			public void mousePressed(MouseEvent arg0) {}
			@Override
			public void mouseReleased(MouseEvent arg0) {}

		});

		//+++++++++++++++++


		// populate GUI with list of Snippets
		if( searchlist != null)
		{
			populateList(0);
		}
		else
			textarea.setText("Vault is empty");


		// code list sorting
		String[] combolist = new String[] {"All","Java","C"};
		comboBox = new JComboBox(combolist);
		comboBox.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent arg0) {
				int choice = comboBox.getSelectedIndex();
				populateList(choice);
			}
		});

		// unused panel. Add something!!
		panelfooter = new JPanel();

		//+++++++++++++++

		//main content pane, holds subpanels
		contentPane.add(panelsearch, BorderLayout.NORTH);
		contentPane.add(panelmain, BorderLayout.CENTER);
		contentPane.add(panelfooter, BorderLayout.SOUTH);

		//searchpanel
		panelsearch.add(txtsearch, BorderLayout.NORTH);
		panelsearch.add(btnNewSnippet, BorderLayout.WEST);
		panelsearch.add(btnRemoveSnippet, BorderLayout.EAST);
		panelsearch.add(comboBox, BorderLayout.CENTER);


	}// end constructor


	//==============================================================================================================

	/**
	 * Given a string name, search the map and send that snippet to showSnippet
	 * 
	 * @param snippetname is the key value for the HashMap
	 */
	private void startSearch( final String snippetname)
	{
		
		Thread thread = new Thread(new Runnable()
		{
			@Override
			public void run() {
				out(snippetname);
				
				if( themap != null && snippetname != null)
				{
					snippet = themap.get(snippetname);
				}
				else if( themap == null || snippet == null)
				{
					themap = new ConcurrentHashMap<String,Snippet>();
				}
				if(snippet == null)
				{
					JOptionPane.showMessageDialog(null, "Check your spelling, no Snippet found");
					return;
				}
				// all good, so show snippet
				showSnippet(snippet);
			}

		});
		thread.start();
	}


	//==============================================================================================================

	/**
	 * Takes the text from the Search box and uses that as the name for a new Snippet
	 */
	private void makeSnippet()
	{
		// grabs a long to store in the Snippet to be transformed into a date with future functionality
		GregorianCalendar gc = new GregorianCalendar();
		gc.setTimeZone( TimeZone.getTimeZone("America/Atlanta"));
		long time = gc.getTimeInMillis();

		// grab text from GUI
		String snippetname = txtsearch.getText();

		// checks for duplicate snippet name
		if(searchlist != null)
		{
			while( searchlist.contains(snippetname))
			{
				snippetname = JOptionPane.showInputDialog("That name is already used.\nChoose another.");
				if(snippetname == null || snippetname == "")
					return;
			}
		}


		// snippet text is stored on clip board
		String snippettext = getClipboard();
		if(snippettext == null || snippettext.equals(""))
		{
			JOptionPane.showMessageDialog(null, "The clipboard is empty");
			return;
		}	

		// create new snippet
		String lang = null;
		if( snippetname.indexOf(' ') != -1)
			lang = snippetname.substring(0, snippetname.indexOf(' '));
		else
			lang = "All";

		Snippet sn = new Snippet(snippetname,time,snippettext,lang);


		// initial snippet if null
		if( themap == null)
			themap = new ConcurrentHashMap<String,Snippet>();


		// display this new snippet
		showSnippet(sn);
	}

	//==============================================================================================================


	/**
	 * Using the passed in snippet, a new instance of Output2 is created to display the snippet text
	 * 
	 * @param sn
	 */
	private void showSnippet(Snippet sn)
	{
		if( sn == null)
		{
			return;
		}

		// search open windows for the snippet the user want to display, & if found move that window to the front
		boolean match = false;
		for(final Output2 s : openwindows)
		{
			if( s.getName().equals(sn.getName()) == true )
			{
				// tell GUI thread to change front window when it gets off its lazy butt and decides to do it
				EventQueue.invokeLater(new Runnable()
				{
					@Override
					public void run() {
						s.toFront();
					}
				});
				// found matching window so stop searching. boolean is to exit this method. the break exits this for loop
				match = true;
				break;
			}
		}
		if( match == true)
			return;

		// makes a new separate window to display the Snippet
		output = new Output2(sn);

		// loads keywords for searching. Did it this way so I can use .contains instead of coding many for loops
		keywordlist = new ArrayList<String>();
		if( sn.getLanguage().equals("Java") == true)
		{
			for(String st : javakeywords)
			{
				keywordlist.add(st);
			}
		}
		else if( sn.getLanguage().equals("C"))
		{
			for(String st : ckeywords)
			{
				keywordlist.add(st);
			}
		}

		//========================================

		if( keywordlist.size() > 0)
		{
			
			String text = sn.getSnippetText();

			StringBuilder word = new StringBuilder();
			StringBuilder comment = new StringBuilder();
			StringBuilder block = new StringBuilder();
			StringBuilder nonalpha = new StringBuilder();
			int count = 0;
	
			while( count < text.length() )
			{
	
				//comment
				if( text.charAt(count) == '/' &&
						(count + 1) < text.length() &&
						text.charAt(count + 1) == '/')
				{
					// check if any code to output before this comment
					if( word.toString().length() > 0)
					{
						output.addText(word.toString(), "regular");
						word = new StringBuilder();
					}
					//check for any punctuation and NPC to output before this comment
					if( nonalpha.toString().length() > 0)
					{
						output.addText(nonalpha.toString(), "regular");
						nonalpha = new StringBuilder();
					}
					//grab other slash from pair of slashes
					comment.append('/');
	
					// add all of comment
					while( text.charAt(count) != '\n')
					{
						//leading count increment to add the newline to the comment before while loop exits
						count++;
						comment.append(text.charAt(count));
					}
					output.addText(comment.toString(), "comment");
					comment = new StringBuilder();
					count++;
					continue;
				}
				
				//block comment
				if( ( (text.charAt(count) == '/') &&// this is a Javadocs style block comment
					(count+1) < text.length() &&
					(text.charAt(count+1) == '*') &&
					(count+2) < text.length() &&
					(text.charAt(count+2) == '*') ) 
					||
					( (text.charAt(count) == '/') &&// this is a C style block comment
					(count+1) < text.length() &&
					(text.charAt(count+1) == '*')) ) 
				{
					// might have code to output before this, such as if(value) this would output the "if"
					if( word.toString().length() > 0)
					{
						if( keywordlist.contains(word.toString()))
						{
							output.addText(word.toString(), "keyword");
						}
						else
						{
							output.addText(word.toString(), "regular");
						}
						word = new StringBuilder();
					}
					if( nonalpha.toString().length() > 0)
					{
						output.addText(nonalpha.toString(), "regular");
						nonalpha = new StringBuilder();
					}
					
					//grab whole block comment
					while( (count+3) < text.length() && (text.substring(count, count+2).equals("*/") == false) )
					{
						block.append(text.charAt(count));
						count++;
					}
					// these 3 lines grab the */\n at the end of a block comment
					block.append(text.charAt(count++));
					block.append(text.charAt(count++));
					block.append(text.charAt(count++));
					
					output.addText(block.toString(), "blockcomment");
					block = new StringBuilder();
					continue;
				}
	
				// grabs non printing characters and punctuation
				if( (Character.isDigit(text.charAt(count)) == false) &&
						(Character.isLetter(text.charAt(count)) == false) )
				{
					// might have code to output before this, such as if(value) this would output the "if"
					if( word.toString().length() > 0)
					{
						if( keywordlist.contains(word.toString()))
						{
							output.addText(word.toString(), "keyword");
						}
						else
						{
							output.addText(word.toString(), "regular");
						}
						word = new StringBuilder();
					}
					nonalpha.append(text.charAt(count));
					count++;
					continue;
				}
	
				//either a keyword or another word
				else
				{
					if( nonalpha.toString().length() > 0)
					{
						output.addText(nonalpha.toString(), "regular");
						nonalpha = new StringBuilder();
					}
					word.append(text.charAt(count));
					if( keywordlist.contains(word.toString()) &&
							(count +1) < text.length() &&
							Character.isLetter(text.charAt(count +1)) == false)
					{
						output.addText(word.toString(), "keyword");
						word = new StringBuilder();
					}
					count++;
				}
			}// end while loop
	
	
			// reached end of input string, so output any unfinished output strings
			if( nonalpha.toString().length() > 0)
				output.addText(nonalpha.toString(), "regular");
	
			if( word.toString().length() > 0)
			{
				if( keywordlist.contains(word.toString()))
				{
					output.addText(word.toString(), "keyword");
				}
				else
				{
					output.addText(word.toString(), "regular");
				}
			}
	
			if( comment.toString().length() > 0)
				output.addText(comment.toString(), "comment");

		}// end coloring if statement
		else
		{
			output.addText(sn.getSnippetText(), "regular");
		}

		output.moveUp();
		
		// add this new window to list of active windows
		updateOpenWindowList(output,false,-1);
		// the act of filling the window "changes" this snippet even though the user made no changes
		output.getSnippet().setChanged(false);

	}


	//==============================================================================================================


	/**
	 * Grabs the text from the clip board to place into the Snippet
	 * 
	 * @return
	 */
	public static String getClipboard() {
		Transferable t = Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);

		try {
			if (t != null && t.isDataFlavorSupported(DataFlavor.stringFlavor)) {
				String text = (String)t.getTransferData(DataFlavor.stringFlavor);
				return text;
			}
		} catch (UnsupportedFlavorException e) {
			out("Error getting text from ClipBoard");
		} catch (IOException e) {
			out("Error IOE in getClipboard");
		}
		return null;
	}




	//==============================================================================================================


	/**
	 * once an open snippet window is closed, remove it from the "active" list.
	 * If the text was changed, it saves those changes in the snippet
	 * 
	 * @param x is the instance of Output
	 * @param changed means the user changed the snippet text
	 */

	protected static void removeSnippet(Output2 x, String newtext)
	{
		if( newtext != null)
		{

			String name = x.getName();
			Snippet snip = themap.get(name);
			if(snip.getSnippetText().equals(newtext) == false)
			{
				snip.setSnippetText(newtext);
				themap.put(name, snip);
			}

		}
		//remove this window from list
		updateOpenWindowList(x,true,-1);

	}
	
	//==============================================================================================================

	/**
	 * The user can have multiple Snippets open at once. The openwindows list keeps track of which are open,
	 * and this method maintains that list.
	 * 
	 * @param x			Snippet window to change
	 * @param remove	remove operation if true, otherwise add
	 * @param index		remove a specific Snippet window by index
	 * @return			the updated list
	 */
	protected static ArrayList<Output2> updateOpenWindowList(Output2 x, boolean remove, int index)
	{
		if( remove == true)
		{
			if( index >= 0)
			{
				openwindows.remove(index);
			}
			else
				openwindows.remove(x);
		}
		// else add to list
		else
		{
			openwindows.add(x);
		}

		return openwindows;
	}

	//==============================================================================================================

	/**
	 * Adds the Snippet to the HashMap
	 * 
	 * @param sn		Snippet to be added
	 * @param newtext	Replace this Snippet's text with newtext
	 */
	protected static void saveSnippet(Snippet sn, String newtext)
	{
		Snippet snip = themap.get(sn.getName());
		String name = sn.getName();

		if( snip == null)
		{
			sn.setSnippetText(newtext);
			themap.put(sn.getName(), sn);	
		}
		else
		{
			snip.setSnippetText(newtext);
			themap.put(snip.getName(), snip);
		}

		if( searchlist == null)
			searchlist = new ArrayList<String>();

		if( searchlist.contains(name) == false)
		{
			searchlist.add(name);
			populateList(comboBox.getSelectedIndex());
		}
		sn.setChanged(false);

	}

	//==============================================================================================================


	/**
	 * Performs the file save/close operations if the user closes the program
	 */
	private void exitProgram()
	{
		exiting = true;
		if( openwindows.size() != 0)
			synchronized(openwindows)
			{
				for(Output2 s : openwindows)
				{
					String newtext = s.getOutputText();
					Snippet sn = s.getSnippet();
					String oldtext = sn.getSnippetText();

					if( (newtext.equals(oldtext) == false) &&
							sn.isChanged() == true )
					{
						JOptionPane jop = new JOptionPane("Save changes to: "+ sn.getName()+"?");
						String[] options = new String[] { "No","Yes"};
						jop.setOptions(options);
						JDialog dialog = jop.createDialog(new JFrame(), "Question");
						dialog.setVisible(true);
						Object returnobject = jop.getValue();
						int result = -1;
						for( int a = 0; a < options.length; a++)
						{
							if( options[a].equals(returnobject))
							{
								result = a;
								break;
							}
						}
						// user wants to save if == 1
						if( result == 1)
						{
							sn.setSnippetText(newtext);
							themap.put(sn.getName(), sn);
						}
						else
						{
							if( searchlist.contains(sn.getName()))
							{
								searchlist.remove(sn.getName());
							}
						}
					}
					s.dispose();
				}
			}
		@SuppressWarnings("unused")
		boolean waitforit = false;

		waitforit = ACV.saveTheData(themap, searchlist);

		this.dispose();

	}

	//==============================================================================================================

	public static String getVaultPath()
	{
		return objectfilepath;
	}

	//==============================================================================================================

	private static void out(String string)
	{
		System.out.println(string);
	}

	//==============================================================================================================

	public static void setMap(ConcurrentHashMap<String,Snippet> map)
	{
		themap = map;
	}

	//==============================================================================================================

	public static ConcurrentHashMap<String,Snippet> getMap()
	{
		return themap;
	}

	//==============================================================================================================

	public static ArrayList<String> getList()
	{
		return searchlist;
	}

	//==============================================================================================================

	public static void setList(ArrayList<String> list)
	{
		searchlist = list;
	}

	//==============================================================================================================

	public static int comboSelection()
	{
		return comboBox.getSelectedIndex();
	}
	
	//==============================================================================================================

	/**
	 * Updates the Snippet list in the main window of the program
	 * 
	 * @param choice controls which type of snippets are displayed
	 */
	public static void populateList(int choice)
	{
		
		textarea.setText("");

		if( searchlist != null && searchlist.size() != 0)
		{
			Collections.sort(searchlist);
			switch(choice)
			{
			case 0:
			{
				for(String st : searchlist)
				{
					textarea.append(st+"\n");
				}
				break;
			}
			case 1:
			{
				for(String st : searchlist)
				{
					if( st.startsWith("Java"))
					{
						textarea.append(st+"\n");
					}
				}
				break;
			}
			case 2:
			{
				for(String st : searchlist)
				{
					if( st.startsWith("C"))
					{
						textarea.append(st+"\n");
					}
				}
				break;
			}
			
			
			}
		}
		else if( themap == null || themap.size() == 0)
			textarea.append("Vault is empty");
	}

	//==============================================================================================================

	/**
	 * Discards the current list of snippets and builds a new list from the contents of the HashMap
	 */
	private void rebuildSearchList()
	{
		searchlist = new ArrayList<String>();
		Collection<Snippet> c = themap.values();
		Iterator<Snippet> itr = c.iterator();
		while( itr.hasNext() )
		{
			searchlist.add(itr.next().getName());
		}
		populateList(0);
		JOptionPane.showMessageDialog(this, "List rebuilt");
	}

	//==============================================================================================================


	/**
	 * Retrieves the path to the Vault, if present in the programs Preference file
	 */
	private void getPrefs()
	{
		//Java stores a little database for each program that we can access. I store the path to the Vault in that.
		Preferences prefs = Preferences.userRoot().node(this.getClass().getName());

		//Java preferences holds the path to the folder where the data is stored
		String folder = prefs.get("objectfilepath", "nullpath");

		//no path stored in prefs, so ask for one from user
		if( folder != null && folder.equals("nullpath") == true)
		{
			updatePrefs();
		}
		//path found
		else
			objectfilepath = folder;
	}


	//==============================================================================================================



	/**
	 * 	Sets the Java Preferences for this program, which is merely the location to put the Vault
	 */
	private void updatePrefs()
	{
		boolean newprefs = false;
		String folder = null;
		if( objectfilepath == null)
		{
			folder = JOptionPane.showInputDialog(null, "Enter the folder you want to store\nthe index files in.\n" +
					"Current setting: None");
			newprefs = true;
		}
		else
		{
			folder = JOptionPane.showInputDialog(null, "Enter the folder you want to store\nthe index files in.\n" +
					"Current setting: " + objectfilepath);
		}
		if( folder == null)//user canceled
			return;
		
		if( folder.equals("clear") )
		{
			Preferences prefs = Preferences.userRoot().node(this.getClass().getName());
			try {
				prefs.clear();
				prefs.flush();
			} catch (BackingStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.exit(0);
		}

		if( folder.contains("\\"))
		{
			String newpath = folder.replace("\\", File.separator);
			folder = newpath;
		}

		if( folder.endsWith(File.separator) == false)
		{
			String temp = folder+File.separator;
			folder = temp;
		}

		//tests the inputed folder
		while( true )
		{
			File fl = new File(folder);
			if( fl != null && fl.exists() && fl.isDirectory() )
			{
				break;
			}
			else
			{
				folder = JOptionPane.showInputDialog(null, "That path appears to be invalid. Try again.");
				if( folder == null)//user canceled change altogether
					return;
			}
		}

		// folder passed tests, so set prefs
		Preferences prefs = Preferences.userRoot().node(this.getClass().getName());
		try {
			prefs.clear();
			prefs.flush();
		} catch (BackingStoreException e1) {
			out("Error in erasing prefs in updatePrefs");
			e1.printStackTrace();
		}

		//commits changes
		objectfilepath = folder;
		prefs.put("objectfilepath", objectfilepath);
		try {
			prefs.sync();
		} catch (BackingStoreException e1) {
			out("Error in syncing new prefs in updatePrefs");
			e1.printStackTrace();
		}
		
		if( newprefs == true)
		{
			JOptionPane.showMessageDialog(null, "Since the Preference field was empty, the program will\n"+
												"need to be restarted so your changes will take effect.");
			System.exit(0);
		}
		
		// clear listing
		searchlist = new ArrayList<String>();
		populateList(0);
	}










}// end of code
