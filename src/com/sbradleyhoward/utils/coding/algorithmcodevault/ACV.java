package com.sbradleyhoward.utils.coding.algorithmcodevault;

/**
 * 	Author: S Bradley Howard (friends call me Brad)
 * 	Website:	sbradleyhoward.com (Password: showme) 
 * 
 */



import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JOptionPane;

public class ACV {


	//Constructor
	public ACV(String filename)
	{
		getTheData(filename);
	}
	
	//==============================================================================================================

	/**
	 * Given the pathtovault, this method opens the needed streams and loads the list of names, and the map, in that order
	 * 
	 * @param pathtovault
	 */
	@SuppressWarnings("unchecked")
	private void getTheData(String pathtovault)
	{
		ObjectInputStream objectinstream = null;
		FileInputStream fileinstream = null;
		
		
		// you may want to add an extension to "algoVault"
		File file = new File(pathtovault + "algoVault");
		
		// if there is not Vault, make an empty one
		if( file.exists() == false)
		{
			try {
				file.createNewFile();
			} catch (IOException e) {
				out("Error creating file");
				e.printStackTrace();
			}
		}
		// get streams to input data
		try {
			fileinstream = new FileInputStream(file);
			objectinstream = new ObjectInputStream(fileinstream);
		} catch (FileNotFoundException e) {
			out("Error with FileInputStream");
			JOptionPane.showMessageDialog(null, "The Vault is emtpy so add come code.");
			
			AlgorithmCodeVault.setMap(null);
			AlgorithmCodeVault.setList(null);

			return;
			//e.printStackTrace();
		} catch (IOException e) {
			out("Error getting ObjectInputStream");
			JOptionPane.showMessageDialog(null, "The Vault is emtpy so add come code.");

			AlgorithmCodeVault.setMap(null);
			AlgorithmCodeVault.setList(null);

			return;
			//e.printStackTrace();
		}

		if( objectinstream != null)
		{
			// streams are good, so load data
			try
			{
				AlgorithmCodeVault.setList( (ArrayList<String>) objectinstream.readObject() );
				AlgorithmCodeVault.setMap( (ConcurrentHashMap<String,Snippet>) objectinstream.readObject() );

			}catch (ClassCastException ce)
			{
				out("Casting error in getTheMap");
				ce.printStackTrace();
			} catch (IOException e) {
				out("IOE in getTheMap");
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				out("ClassNotFound in getTheMap");
				e.printStackTrace();
			}
		}

		// data retrieved successfully, so close streams
		try
		{
			objectinstream.close();
			fileinstream.close();
		} catch (IOException e) {
			out("Error closing streams");
			e.printStackTrace();
		}

	}

	//==============================================================================================================
	
	/**
	 * When the user closes the program, this method saves any changes made, and closes the files
	 * 
	 * @param map stores the snippets and the arraylist of snippet names
	 * @param list is arraylist of snippet names
	 * @return the return on this is not used
	 */
	protected static boolean saveTheData(ConcurrentHashMap<String,Snippet> map, ArrayList<String> list)
	{
		ObjectOutputStream objectoutstream = null;
		FileOutputStream fileoutstream = null;

		File file = new File(AlgorithmCodeVault.getVaultPath() + File.separator + "AlgoVault");
		
		//user deleted all of the Vault, one entry at a time
		if( map == null || list == null)
		{
			return false;
		}

		// set up output streams
		try {
			file.createNewFile();
			fileoutstream = new FileOutputStream(file);
			objectoutstream = new ObjectOutputStream(fileoutstream);
		} catch (IOException e) {
			out("Error creating file");
			e.printStackTrace();
		}

		// streams good, so write data
		try {
			objectoutstream.writeObject(list);
			objectoutstream.writeObject(map);
		} catch (IOException e) {
			out("Error writing objects");
			e.printStackTrace();
		}

		//data written, so close streams
		try {
			objectoutstream.close();
			fileoutstream.close();
		} catch (IOException e) {
			out("Error closing files");
			e.printStackTrace();
		}
		
		return false;
	}

	//==============================================================================================================


	private static void out(String string)
	{
		System.out.println(string);
	}
	
	//==============================================================================================================
}
