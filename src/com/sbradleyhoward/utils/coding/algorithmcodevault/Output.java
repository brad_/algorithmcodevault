package com.sbradleyhoward.utils.coding.algorithmcodevault;



import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;
import javax.swing.text.TabSet;
import javax.swing.text.TabStop;



/**
 * 	This class is only to output the results of all the other classes
 * 	to a simple GUI
 * 
 */

public class Output extends JFrame implements WindowListener
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextPane pane = new JTextPane();
	private StyledDocument doc;
	private Style regular;
	private Style keyword;
	private Style comment;
	private static int offset;
	private String snippetname = null;
//	private boolean changed = false;
	private int inserts = -1;
	private int removes = 0;
	private Snippet snip = null;
//	private final Output finalthis = this;
	
	//==============================================================================================================
	
	// when user closes window, checks if any changes have been made to Snippet text
	
	@Override
	public void windowClosed(WindowEvent arg0) {
		// ignoring changes here if user is closing program with more than one snippet open
		if( Acvgui.exiting == true)
			return;
		
		// changes made, but user is only closing THIS ONE snippet
		if( removes > 0 || inserts > 0)
		{
			Acvgui.removeSnippet(this,pane.getText());
		}
		// no changes made
		else
		{
			Acvgui.removeSnippet(this,null);
		}
	}
	
	
	// unused overrides
	@Override
	public void windowClosing(WindowEvent arg0) {}
	@Override
	public void windowActivated(WindowEvent arg0) {}
	@Override
	public void windowDeactivated(WindowEvent arg0) {}
	@Override
	public void windowDeiconified(WindowEvent arg0) {}
	@Override
	public void windowIconified(WindowEvent arg0) {}
	@Override
	public void windowOpened(WindowEvent arg0) {}
	
	//==============================================================================================================

//	public Output(String name)
	public Output(Snippet snip)
	{
		snippetname = snip.getName();
		this.snip = snip;
		
//		snippetname = name;
		this.setTitle(snippetname);
		
		pane.setMargin(new Insets(5,10,15,10));
		pane.setBorder(BorderFactory.createTitledBorder("Algorithm Code Snippet"));

		doc = pane.getStyledDocument();
		pane.setStyledDocument(doc);
		
		// listens for the user changing the snippet text
		pane.getDocument().addDocumentListener(new DocumentListener()
		{
			@Override
			public void insertUpdate(DocumentEvent ev) {
				inserts++;	
			}
			@Override
			public void removeUpdate(DocumentEvent ev) {
				removes++;	
			}
			@Override
			public void changedUpdate(DocumentEvent ev) {}
		});
		
		// when user clicks inside of the Snippet viewer window, this enables editing
		pane.addMouseListener(new MouseListener()
		{
			@Override
			public void mousePressed(MouseEvent arg0) {
				pane.setEditable(true);
				pane.getCaret().setVisible(true);
			}

			// unused overrides
			@Override
			public void mouseClicked(MouseEvent click) {}
			@Override
			public void mouseEntered(MouseEvent arg0) {}
			@Override
			public void mouseExited(MouseEvent arg0) {}
			@Override
			public void mouseReleased(MouseEvent arg0) {}			
		});

		Style defaultstyle = StyleContext.getDefaultStyleContext().getStyle( StyleContext.DEFAULT_STYLE);

		// formatting for the snippet viewer
		TabStop[] tabs = new TabStop[4];
		tabs[0] = new TabStop(40,TabStop.ALIGN_RIGHT,TabStop.LEAD_NONE);
		tabs[1] = new TabStop(80,TabStop.ALIGN_RIGHT,TabStop.LEAD_NONE);
		tabs[2] = new TabStop(120,TabStop.ALIGN_RIGHT,TabStop.LEAD_NONE);
		tabs[3] = new TabStop(160,TabStop.ALIGN_RIGHT,TabStop.LEAD_NONE);
		TabSet tabset = new TabSet(tabs);
		StyleContext sc = StyleContext.getDefaultStyleContext();
		AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY,
				StyleConstants.TabSet, tabset);
		pane.setParagraphAttributes(aset, false);



		regular = doc.addStyle("regular", defaultstyle);

		keyword = doc.addStyle("keyword", regular);
		StyleConstants.setForeground(keyword, Color.magenta);

		comment = doc.addStyle("comment", regular);
		StyleConstants.setForeground(comment, Color.green);

		

		
		
		this.getContentPane().add( new JScrollPane( this.pane));
		
		// puts viewer in center of screen. Offset merely "tiles" the windows
		Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize(800,600);
		this.setLocation(dimension.width / 2 - 400+offset, dimension.height / 2 - 200+offset);
		this.setDefaultCloseOperation( JFrame.DISPOSE_ON_CLOSE);
		this.setVisible(true);
		offset +=20;
		if( offset > 99)
			offset = 0;

		this.addWindowListener(this);
	}
	
	//==============================================================================================================


	public void addText(String string, String type)
	{
		string.concat("\n");
		if( type == "regular")
			try {
				doc.insertString(doc.getLength(), string, regular);
			} catch (BadLocationException e) {
				out("Error due to bad location in addText");
				e.printStackTrace();
			}
		else if( type == "keyword")
		{
			try {
				doc.insertString(doc.getLength(), string, keyword);
			} catch (BadLocationException e) {
				out("Error due to bad location in addText");
				e.printStackTrace();
			}
		}
		else
		{
			try {
				doc.insertString(doc.getLength(), string, comment);
			} catch (BadLocationException e) {
				out("Error due to bad location in addText");
				e.printStackTrace();
			}
		}
	}

	//==============================================================================================================

	public void moveUp()
	{
		pane.setCaretPosition(0);
	}
	
	//==============================================================================================================
	
	public String getOutputText()
	{
		return pane.getText();
	}
	
	//==============================================================================================================
	

	private void out(String string)
	{
		System.out.println(string);
	}
	
	//==============================================================================================================

	public String getName()
	{
		return snippetname;
	}
	
	//==============================================================================================================
	
	public Snippet getSnippet()
	{
		return snip;
	}


}

